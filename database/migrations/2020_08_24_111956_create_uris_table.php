<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUrisTable extends Migration
{
    public function up()
    {
        Schema::create('uris', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('size')->index();
            $table->string('minified')->index();
            $table->string('original')->unique();
            $table->bigInteger('redirects')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('uris');
    }
}
