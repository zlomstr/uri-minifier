<table>
<tr><td>
<b>Get uri redirects:</b>
<br/><code>GET	api/default/uri-redirects</code>
<br/>query params:
<br/>`minified` string
</td></tr>

<tr><td>
<b>Get uri count, default all:</b>
<br/><code>GET	api/default/uri-counts</code>
<br/>query params:
<br/>`size` integer
</td></tr>

<tr><td>
<b>Minified uri:</b>
<br/><code>POST	api/default/uris</code>
<br/>query params:
<br/>`original` string, must be valid URI
</td></tr>
</table>

