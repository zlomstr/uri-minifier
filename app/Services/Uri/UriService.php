<?php

namespace App\Services\Uri;

use App\Models\Uri;
use Exception;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;

class UriService
{
    protected $model;

    public function __construct(Uri $model)
    {
        $this->model = $model;
    }

    public function redirectIfExist($minified)
    {
        $uri = $this->getMinifiedBuilder($minified)->firstOrFail();

        $uri->increment('redirects');

        return Redirect::to($uri->original)->header('Cache-Control', 'no-store, no-cache, must-revalidate');
    }

    public function store($original)
    {
        $minified = $this->minified();

        $model = $this->model->newQuery()->firstOrNew([
            'original' => $this->prepareOriginalUri($original),
        ], [
            'size' => Str::length($minified),
            'minified' => $minified,
        ]);

        if (! $model->isDirty()) {
            throw new Exception('This URI already exist.');
        }

        $model->save();

        return $model;
    }

    public function getRedirects($minified)
    {
        return $this->getMinifiedBuilder($minified)
            ->firstOrFail()
            ->getAttribute('redirects');
    }

    public function getCounts($size)
    {
        $query = Uri::query();
        if ($size > 0) {
            $query->where('size', $size);
        }

        return $query->count();
    }

    protected function minified()
    {
        $minified = Str::lower(Str::substr(Str::random(9), 0, rand(3, 6)));

        $uri = $this->getMinifiedBuilder($minified)->first();

        if ($uri) {
            return $this->minified();
        }

        return $minified;
    }

    protected function getMinifiedBuilder($minified)
    {
        return $this->model->newQuery()
            ->where('minified', Str::lower($minified));
    }

    protected function prepareOriginalUri($original)
    {
        $data = parse_url($original);

        $host = $data['host'] ?? null;
        $path = $data['path'] ?? null;
        $query = $data['query'] ?? null;

        // Cutting `www.` from original host because host with `www` equal without `www`.
        if (Str::is('www.*', $host)) {
            $host = Str::substr($host, 4, Str::length($host));
        }

        return 'https://'.$host.$path.$query;
    }
}
