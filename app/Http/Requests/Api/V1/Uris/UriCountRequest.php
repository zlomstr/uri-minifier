<?php

namespace App\Http\Requests\Api\V1\Uris;

use Illuminate\Foundation\Http\FormRequest;

class UriCountRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'size' => 'integer',
        ];
    }
}
