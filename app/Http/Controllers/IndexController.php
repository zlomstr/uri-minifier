<?php

namespace App\Http\Controllers;

use App\Services\Uri\UriService;

class IndexController extends Controller
{
    public function __invoke($modified, UriService $uriService)
    {
        return $uriService->redirectIfExist($modified);
    }
}
