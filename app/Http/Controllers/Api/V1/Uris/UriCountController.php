<?php

namespace App\Http\Controllers\Api\V1\Uris;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\Uris\UriCountRequest;
use App\Http\Resources\ModelResource;
use App\Services\Uri\UriService;

class UriCountController extends Controller
{
    public function __invoke(UriCountRequest $request, UriService $uriService)
    {
        return new ModelResource($uriService->getCounts($request->get('size')));
    }
}
