<?php

namespace App\Http\Controllers\Api\V1\Uris;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\Uris\UriRequest;
use App\Http\Resources\ModelResource;
use App\Services\Uri\UriService;
use Exception;

class UriController extends Controller
{
    public function store(UriRequest $request, UriService $uriService)
    {
        try {
            return new ModelResource($uriService->store($request->get('original')));
        } catch (Exception $exception) {
            return redirect('/')->withErrors(['original' => $exception->getMessage()]);
        }
    }
}
