<?php

namespace App\Http\Controllers\Api\V1\Uris;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\Uris\RedirectRequest;
use App\Http\Resources\ModelResource;
use App\Services\Uri\UriService;

class UriRedirectController extends Controller
{
    public function __invoke(RedirectRequest $request, UriService $uriService)
    {
        return new ModelResource($uriService->getRedirects($request->get('minified')));
    }
}
