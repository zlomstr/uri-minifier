<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/{minified}', 'IndexController');

Route::group(['prefix' => 'api', 'name' => 'api.', 'namespace' => 'Api'], function () {
    Route::group(['prefix' => 'v1', 'name' => 'v1.', 'namespace' => 'V1'], function () {
        Route::post('uris', 'Uris\UriController@store')->name('uris.store');

        Route::get('uri-counts', 'Uris\UriCountController');
        Route::get('uri-redirects', 'Uris\UriRedirectController');
    });
});
